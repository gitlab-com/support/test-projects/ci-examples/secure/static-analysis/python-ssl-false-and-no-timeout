# python-ssl-false-and-no-timeout



## SUMMARY

This is a simple python project where semgrep runs and reports 2 vulnerabilties:

- Improper certificate validation (CWE-295 | https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/ssl/rule-req-no-certvalid.yml)

- Uncontrolled resource consumption (CWE-400 | https://gitlab.com/gitlab-org/security-products/sast-rules/-/blob/main/python/requests/rule-request-without-timeout.yml)



